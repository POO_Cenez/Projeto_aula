package br.unifor.poo.model;

import java.util.ArrayList;
import java.util.List;

public class Departamento {
	private int codigo;
	private String nome;
	public final List<Funcionario> funcionarios = new ArrayList<Funcionario>();
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void solicitaFuncionario(Funcionario gestor, Funcionario funcionarioSolicitado) {
		if(gestor instanceof IGestaoDePessoas)
			((IGestaoDePessoas)gestor).visitar(this, funcionarioSolicitado, IGestaoDePessoas.ACTION.ALOCAR);
	}
	public void dispensaFuncionario(Funcionario gestor, Funcionario funcionarioDispensado) {
		if(gestor instanceof IGestaoDePessoas)
			((IGestaoDePessoas)gestor).visitar(this, funcionarioDispensado, IGestaoDePessoas.ACTION.DESALOCAR);
	}
	public void listaColaboradores() {
		for (Funcionario funcionario : funcionarios) {
			System.out.println(funcionario + ":"+funcionario.getDepartamento().getNome());
		}
	}
	@Override
	public String toString() {
		return this.codigo+":"+this.nome;
	}
	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}
	public void setFuncionarios(List<Funcionario> lista) {
		this.funcionarios.clear();
		for (Funcionario funcionario : lista) {
			this.funcionarios.add(funcionario);
		}
	}
}
