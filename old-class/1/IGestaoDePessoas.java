package br.unifor.poo.model;

public interface IGestaoDePessoas {
	public static enum ACTION {ALOCAR, DESALOCAR, CONTRATAR, EXONERAR; }
	public void visitar(Funcionario funcionario, ACTION acao);
	public void visitar(Departamento departamento, Funcionario funcionario, ACTION acao);
}
