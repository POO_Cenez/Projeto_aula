package br.unifor.poo.model;

public class Funcionario {
	
	private int matricula;
	private String nome;
	private String atribuicao;
	private Departamento departamento = null;
	
	public Funcionario() {}
	
	public int getMatricula() {
		return matricula;
	}
	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getAtribuicao() {
		return atribuicao;
	}
	public void setAtribuicao(String atribuicao) {
		this.atribuicao = atribuicao;
	}
	public Departamento getDepartamento() {
		return departamento;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Funcionario) {
			Funcionario f = (Funcionario) obj;
			if(f.getMatricula()==this.matricula)
				return true;
		}
		return false;
	}
	@Override
	public String toString() {
		return this.matricula+"."+this.nome;
		//return this.matricula+":"+this.nome+":"+this.atribuicao+":"+this.departamento.getNome();
	}
}
