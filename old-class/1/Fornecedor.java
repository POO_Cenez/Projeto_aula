package br.unifor.poo.model;

public class Fornecedor {
	
	private Long id;
	private String nome;
	private String descricao;
	
	public Fornecedor() { }

	public Fornecedor(Long l) {
		id = l;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Fornecedor) {
			Fornecedor f = new Fornecedor();
			if(f.getId()==this.id) return true;
		}
		return false;
	}
	@Override
	public String toString() {
		return "id=" + id + " nome=" + nome + " descricao=" + descricao;
	}
	public Long cnpj() { return id; }
	public String razaoSocial() { return nome; }
}
