package br.unifor.poo.model;

import java.util.List;

public class GerenteRH extends Funcionario implements IGestaoDePessoas {
	@Override
	public void visitar(Funcionario funcionario, ACTION acao) {
		this.defineAcoes(null, funcionario, acao);
	}
	@Override
	public void visitar(Departamento departamento, Funcionario funcionario, ACTION acao) {
		this.defineAcoes(departamento, funcionario, acao);
	}
	
	private void defineAcoes(Departamento departamento, Funcionario funcionario, ACTION acao){
		switch (acao) {
		case ALOCAR:
			this.alocaFuncionario(departamento, funcionario);
			break;
		case DESALOCAR:
			this.desalocaFuncionario(departamento, funcionario);
			break;
		case CONTRATAR:
			this.contrata(funcionario);
			break;
		case EXONERAR:
			this.destrato(funcionario);
			break;
		}
	}
	
	private void contrata(Funcionario funcionario) { }
	private void destrato(Funcionario funcionario) { }

	private boolean alocaFuncionario(Departamento departamento, Funcionario funcionario) {
		if(departamento==null) return false;
		List<Funcionario> listaFunc = departamento.getFuncionarios();
		if(!listaFunc.contains(funcionario) && funcionario.getMatricula()>=0) {
			Departamento anterior = funcionario.getDepartamento();
			if(anterior!=null) 
				desalocaFuncionario(anterior, funcionario);
			listaFunc.add(funcionario);
			funcionario.setDepartamento(departamento);
			return true;
		}
		else return false;
	}

	private void desalocaFuncionario(Departamento departamento, Funcionario funcionario) {
		if(departamento!=null) {
			List<Funcionario> listaFunc = departamento.getFuncionarios();
			if(listaFunc.contains(funcionario) && funcionario.getMatricula()>=0) {
				listaFunc.remove(funcionario);
				funcionario.setDepartamento(null);
			}
		}
	}
}
