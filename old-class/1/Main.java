package br.unifor.poo.view;

import java.io.IOException;
import java.sql.SQLException;

import br.unifor.poo.model.Departamento;
import br.unifor.poo.model.Funcionario;
import br.unifor.poo.model.GerenteRH;
import br.unifor.poo.util.GeradorDeXML;

public class Main {
	public static void main(String[] args) throws SQLException, IOException {
		GeradorDeXML.geraDados();
		//Principal p = new Principal();
		//p.montaTela();

		Funcionario a = new Funcionario(); a.setMatricula(1); a.setNome("Manuel da Silva"); a.setAtribuicao("Professor");
		Funcionario b = new Funcionario(); b.setMatricula(2); b.setNome("Pedro Santos"); b.setAtribuicao("Programador");
		
		if(a.equals(b))
			System.out.println("São Iguais");
		else System.out.println("São Diferentes");
		
		Departamento computacao = new Departamento();
		Departamento fisica = new Departamento();
		Funcionario rhGerente = new GerenteRH();
		
		
		computacao.setNome("Computacao");
		fisica.setNome("Fisica");
		
		computacao.solicitaFuncionario(rhGerente, a);
		computacao.solicitaFuncionario(rhGerente, b);
		
		fisica.solicitaFuncionario(rhGerente, b);
		
		computacao.listaColaboradores();
		fisica.listaColaboradores();
		
	}
}
